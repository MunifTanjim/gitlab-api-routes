const fs = require('fs')
const path = require('path')
const marked = require('marked')
const { JSDOM } = require('jsdom')

const {
  getContentBlocks,
  getBlockName,
  getBlockDescription,
  getBlockUrls,
  getBlockParams,
  getBlockPathDeprecation
} = require('./helpers')

const markdownDir = path.resolve('cache/doc/api')
const routesDir = path.resolve('routes')

const EXCLUDED_MARKDOWNS = ['README.md', 'oauth2.md']

const MOVED_MARKDOWNS = {
  'build_triggers.md': 'pipeline_triggers.md',
  'builds.md': 'jobs.md'
}

const markdownFileNames = fs
  .readdirSync(markdownDir)
  .filter(filename => path.extname(filename) === '.md')
  .filter(filename => !EXCLUDED_MARKDOWNS.includes(filename))
  .map(filename => MOVED_MARKDOWNS[filename] || filename)
  .sort()

const markdownFilePaths = markdownFileNames.map(
  filename => `${markdownDir}/${filename}`
)

const routesFileNames = markdownFileNames.map(filename =>
  filename.replace('.md', '.json')
)

markdownFilePaths.forEach((markdownFilePath, i) => {
  fs.readFile(markdownFilePath, (err, data) => {
    if (err) console.error(err)

    let content = data.toString()

    let blocks = getContentBlocks(content)
      .map(markdown => ({ markdown }))
      .map(block => ({
        html: marked.parse(block.markdown),
        ...block
      }))
      .map(block => ({
        dom: JSDOM.fragment(block.html),
        ...block
      }))

    blocks.map(block => {
      block.name = getBlockName(block)
      block.description = getBlockDescription(block)
      block.deprecated = getBlockPathDeprecation(block)
      block.urls = getBlockUrls(block)
      block.params = getBlockParams(block)

      delete block.dom

      return block
    })

    let routes = blocks.reduce((routes, block) => {
      return routes.concat(
        block.urls.map(url => {
          let [method, path] = url.split(' ')

          let endpointObject = {
            name: block.name,
            description: block.description,
            method,
            path,
            params: block.params
          }

          if (block.deprecated) {
            endpointObject.deprecated = true
          }

          return endpointObject
        })
      )
    }, [])

    fs.writeFileSync(
      `${routesDir}/${routesFileNames[i]}`,
      JSON.stringify(routes, null, 2)
    )
  })
})
