const marked = require('marked')
const { JSDOM } = require('jsdom')

const METHODS = ['DELETE', 'GET', 'POST', 'PUT']

let booleanMap = {
  yes: true,
  no: false,
  required: true,
  optional: false,
  undefined: false
}

let parameterKeysMap = {
  attribute: 'name'
}

const regexpEscape = string => {
  return string.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&')
}

const getContentBlocks = content => {
  let blocks = []

  let regex = RegExp(`(?:^|\\n)#{1,3} .+`, 'g')

  let nameMatches = content.match(regex)

  content = content.replace(/^---\s[\S\s]+?---\s/, '').trim()

  if (nameMatches) {
    let names = nameMatches.map(name => name.trim())

    blocks = names
      .map((_name, i) => {
        let blockContent = content.slice(0, content.indexOf(names[i + 1]))

        content = content.slice(content.indexOf(names[i + 1]))

        return blockContent
      })
      .filter(Boolean)
      .filter(block => {
        return RegExp(
          `\`{3}(?:http|plaintext)?\\n(?:${METHODS.join('|')})`
        ).test(block)
      })
  }

  return blocks
}

const getBlockName = ({ markdown }) => {
  let name = /^#{1,3} (.+)/g.exec(markdown)

  if (!name) {
    // console.log(markdown)
  }

  return name[1]
}

const getBlockDescription = ({ markdown, name }) => {
  let regex = RegExp(
    `${regexpEscape(
      name
    )}([\\s\\S]*?)\`{3}(?:http|plaintext)?\\n(?:${METHODS.join('|')})`
  )
  let description = regex.exec(markdown)

  return description
    ? description
        .splice(1)[0]
        .trim()
        .split('\n')
        .join(' ')
    : ''
}

const getBlockUrls = ({ dom }) => {
  let pathsPreCode = dom.querySelector('pre code')
  return pathsPreCode ? pathsPreCode.textContent.split('\n') : []
}

const getBlockParams = ({ dom, markdown }) => {
  let parameters = []

  let table = dom.querySelector('table')

  if (table) {
    let keys = [...table.querySelectorAll('thead tr th')].map(prop =>
      prop.textContent.toLowerCase()
    )
    let rows = [...table.querySelectorAll('tbody tr')].map(row =>
      [...row.querySelectorAll('td')].map(value => value.textContent)
    )

    parameters = rows.reduce((params, row) => {
      return params.concat(
        row.reduce((param, value, i) => {
          let key = parameterKeysMap[keys[i]] || keys[i]

          value = key === 'required' ? booleanMap[value] : value

          if (key === 'description' && /Deprecated/.test(value)) {
            param.deprecated = true
          }

          param[key] = value

          return param
        }, {})
      )
    }, [])
  } else {
    let mark = `Parameters:\n`
    let hasParams = RegExp(mark).test(markdown)

    if (hasParams) {
      let content = markdown.slice(markdown.indexOf(mark))

      let dom = JSDOM.fragment(marked.parse(content))

      let ul = dom.querySelector('ul')

      parameters = [...ul.querySelectorAll('li')].reduce((params, param) => {
        if (param.textContent === 'none') return params

        let item = {}

        let [nameAndRequired, description] = param.textContent.split(' - ')

        let [name, required] = /(\w+) \(?(\w+)?\)?/
          .exec(nameAndRequired)
          .splice(1)

        required = booleanMap[required] || required

        item.name = name
        item.description = description
        item.required = required

        if (/Deprecated/.test(description)) {
          item.deprecated = true
        }

        return params.concat(item)
      }, [])
    }
  }
  return parameters
}

const getBlockPathDeprecation = ({ name, description }) => {
  return (
    /Deprecated/.test(name) ||
    /Deprecated/.test(description) ||
    /api endpoint is deprecated/i.test(description)
  )
}

module.exports.getContentBlocks = getContentBlocks
module.exports.getBlockName = getBlockName
module.exports.getBlockDescription = getBlockDescription
module.exports.getBlockUrls = getBlockUrls
module.exports.getBlockParams = getBlockParams
module.exports.getBlockPathDeprecation = getBlockPathDeprecation
