[
  {
    "name": "List repository branches",
    "description": "Get a list of repository branches from a project, sorted by name alphabetically.  NOTE: **Note:** This endpoint can be accessed without authentication if the repository is publicly accessible.",
    "method": "GET",
    "path": "/projects/:id/repository/branches",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      },
      {
        "name": "search",
        "type": "string",
        "required": false,
        "description": "Return list of branches containing the search string. You can use ^term and term$ to find branches that begin and end with term respectively."
      }
    ]
  },
  {
    "name": "Get single repository branch",
    "description": "Get a single project repository branch.  NOTE: **Note:** This endpoint can be accessed without authentication if the repository is publicly accessible.",
    "method": "GET",
    "path": "/projects/:id/repository/branches/:branch",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      },
      {
        "name": "branch",
        "type": "string",
        "required": true,
        "description": "Name of the branch."
      }
    ]
  },
  {
    "name": "Create repository branch",
    "description": "Create a new branch in the repository.",
    "method": "POST",
    "path": "/projects/:id/repository/branches",
    "params": [
      {
        "name": "id",
        "type": "integer",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      },
      {
        "name": "branch",
        "type": "string",
        "required": true,
        "description": "Name of the branch."
      },
      {
        "name": "ref",
        "type": "string",
        "required": true,
        "description": "Branch name or commit SHA to create branch from."
      }
    ]
  },
  {
    "name": "Delete repository branch",
    "description": "Delete a branch from the repository.  NOTE: **Note:** In the case of an error, an explanation message is provided.",
    "method": "DELETE",
    "path": "/projects/:id/repository/branches/:branch",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      },
      {
        "name": "branch",
        "type": "string",
        "required": true,
        "description": "Name of the branch."
      }
    ]
  },
  {
    "name": "Delete merged branches",
    "description": "Will delete all branches that are merged into the project's default branch.  NOTE: **Note:** [Protected branches](../user/project/protected_branches.md) will not be deleted as part of this operation.",
    "method": "DELETE",
    "path": "/projects/:id/repository/merged_branches",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      }
    ]
  }
]