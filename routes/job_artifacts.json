[
  {
    "name": "Get job artifacts",
    "description": "> The use of `CI_JOB_TOKEN` in the artifacts download API was [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/2346) in [GitLab Premium](https://about.gitlab.com/pricing/) 9.5.  Get the job's artifacts zipped archive of a project.",
    "method": "GET",
    "path": "/projects/:id/jobs/:job_id/artifacts",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      },
      {
        "name": "job_id",
        "type": "integer",
        "required": true,
        "description": "ID of a job."
      },
      {
        "name": "job_token (PREMIUM)",
        "type": "string",
        "required": false,
        "description": "To be used with triggers for multi-project pipelines. It should be invoked only inside .gitlab-ci.yml. Its value is always $CI_JOB_TOKEN."
      }
    ]
  },
  {
    "name": "Download the artifacts archive",
    "description": "> The use of `CI_JOB_TOKEN` in the artifacts download API was [introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/2346) in [GitLab Premium](https://about.gitlab.com/pricing/) 9.5.  Download the artifacts zipped archive from the latest successful pipeline for the given reference name and job, provided the job finished successfully. This is the same as [getting the job's artifacts](#get-job-artifacts), but by defining the job's name instead of its ID.  NOTE: **Note:** If a pipeline is [parent of other child pipelines](../ci/parent_child_pipelines.md), artifacts are searched in hierarchical order from parent to child. For example, if both parent and child pipelines have a job with the same name, the artifact from the parent pipeline will be returned.",
    "method": "GET",
    "path": "/projects/:id/jobs/artifacts/:ref_name/download?job=name",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      },
      {
        "name": "ref_name",
        "type": "string",
        "required": true,
        "description": "Branch or tag name in repository. HEAD or SHA references are not supported."
      },
      {
        "name": "job",
        "type": "string",
        "required": true,
        "description": "The name of the job."
      },
      {
        "name": "job_token (PREMIUM)",
        "type": "string",
        "required": false,
        "description": "To be used with triggers for multi-project pipelines. It should be invoked only inside .gitlab-ci.yml. Its value is always $CI_JOB_TOKEN."
      }
    ]
  },
  {
    "name": "Download a single artifact file by job ID",
    "description": "> Introduced in GitLab 10.0  Download a single artifact file from a job with a specified ID from within the job's artifacts zipped archive. The file is extracted from the archive and streamed to the client.",
    "method": "GET",
    "path": "/projects/:id/jobs/:job_id/artifacts/*artifact_path",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      },
      {
        "name": "job_id",
        "type": "integer",
        "required": true,
        "description": "The unique job identifier."
      },
      {
        "name": "artifact_path",
        "type": "string",
        "required": true,
        "description": "Path to a file inside the artifacts archive."
      }
    ]
  },
  {
    "name": "Download a single artifact file from specific tag or branch",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/23538) in GitLab 11.5.  Download a single artifact file for a specific job of the latest successful pipeline for the given reference name from within the job's artifacts archive. The file is extracted from the archive and streamed to the client.  In [GitLab 13.5](https://gitlab.com/gitlab-org/gitlab/-/issues/201784) and later, artifacts for [parent and child pipelines](../ci/parent_child_pipelines.md) are searched in hierarchical order from parent to child. For example, if both parent and child pipelines have a job with the same name, the artifact from the parent pipeline is returned.",
    "method": "GET",
    "path": "/projects/:id/jobs/artifacts/:ref_name/raw/*artifact_path?job=name",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      },
      {
        "name": "ref_name",
        "type": "string",
        "required": true,
        "description": "Branch or tag name in repository. HEAD or SHA references are not supported."
      },
      {
        "name": "artifact_path",
        "type": "string",
        "required": true,
        "description": "Path to a file inside the artifacts archive."
      },
      {
        "name": "job",
        "type": "string",
        "required": true,
        "description": "The name of the job."
      }
    ]
  },
  {
    "name": "Keep artifacts",
    "description": "Prevents artifacts from being deleted when expiration is set.",
    "method": "POST",
    "path": "/projects/:id/jobs/:job_id/artifacts/keep",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project owned by the authenticated user."
      },
      {
        "name": "job_id",
        "type": "integer",
        "required": true,
        "description": "ID of a job."
      }
    ]
  },
  {
    "name": "Delete artifacts",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/25522) in GitLab 11.9.  Delete artifacts of a job.",
    "method": "DELETE",
    "path": "/projects/:id/jobs/:job_id/artifacts",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "ID or URL-encoded path of the project"
      },
      {
        "name": "job_id",
        "type": "integer",
        "required": true,
        "description": "ID of a job."
      }
    ]
  }
]