[
  {
    "name": "List repository commits",
    "description": "Get a list of repository commits in a project.",
    "method": "GET",
    "path": "/projects/:id/repository/commits",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "ref_name",
        "type": "string",
        "required": false,
        "description": "The name of a repository branch, tag or revision range, or if not given the default branch"
      },
      {
        "name": "since",
        "type": "string",
        "required": false,
        "description": "Only commits after or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ"
      },
      {
        "name": "until",
        "type": "string",
        "required": false,
        "description": "Only commits before or on this date will be returned in ISO 8601 format YYYY-MM-DDTHH:MM:SSZ"
      },
      {
        "name": "path",
        "type": "string",
        "required": false,
        "description": "The file path"
      },
      {
        "name": "all",
        "type": "boolean",
        "required": false,
        "description": "Retrieve every commit from the repository"
      },
      {
        "name": "with_stats",
        "type": "boolean",
        "required": false,
        "description": "Stats about each commit will be added to the response"
      },
      {
        "name": "first_parent",
        "type": "boolean",
        "required": false,
        "description": "Follow only the first parent commit upon seeing a merge commit"
      },
      {
        "name": "order",
        "type": "string",
        "required": false,
        "description": "List commits in order. Possible values: default, topo. Defaults to default, the commits are shown in reverse chronological order."
      }
    ]
  },
  {
    "name": "Create a commit with multiple files and actions",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/6096) in GitLab 8.13.  Create a commit by posting a JSON payload",
    "method": "POST",
    "path": "/projects/:id/repository/commits",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project"
      },
      {
        "name": "branch",
        "type": "string",
        "required": true,
        "description": "Name of the branch to commit into. To create a new branch, also provide either start_branch or start_sha, and optionally start_project."
      },
      {
        "name": "commit_message",
        "type": "string",
        "required": true,
        "description": "Commit message"
      },
      {
        "name": "start_branch",
        "type": "string",
        "required": false,
        "description": "Name of the branch to start the new branch from"
      },
      {
        "name": "start_sha",
        "type": "string",
        "required": false,
        "description": "SHA of the commit to start the new branch from"
      },
      {
        "name": "start_project",
        "type": "integer/string",
        "required": false,
        "description": "The project ID or URL-encoded path of the project to start the new branch from. Defaults to the value of id."
      },
      {
        "name": "actions[]",
        "type": "array",
        "required": true,
        "description": "An array of action hashes to commit as a batch. See the next table for what attributes it can take."
      },
      {
        "name": "author_email",
        "type": "string",
        "required": false,
        "description": "Specify the commit author's email address"
      },
      {
        "name": "author_name",
        "type": "string",
        "required": false,
        "description": "Specify the commit author's name"
      },
      {
        "name": "stats",
        "type": "boolean",
        "required": false,
        "description": "Include commit stats. Default is true"
      },
      {
        "name": "force",
        "type": "boolean",
        "required": false,
        "description": "When true overwrites the target branch with a new commit based on the start_branch or start_sha"
      }
    ]
  },
  {
    "name": "Get a single commit",
    "description": "Get a specific commit identified by the commit hash or name of a branch or tag.",
    "method": "GET",
    "path": "/projects/:id/repository/commits/:sha",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit hash or name of a repository branch or tag"
      },
      {
        "name": "stats",
        "type": "boolean",
        "required": false,
        "description": "Include commit stats. Default is true"
      }
    ]
  },
  {
    "name": "Get references a commit is pushed to",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/15026) in GitLab 10.6  Get all references (from branches or tags) a commit is pushed to. The pagination parameters `page` and `per_page` can be used to restrict the list of references.",
    "method": "GET",
    "path": "/projects/:id/repository/commits/:sha/refs",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit hash"
      },
      {
        "name": "type",
        "type": "string",
        "required": false,
        "description": "The scope of commits. Possible values branch, tag, all. Default is all."
      }
    ]
  },
  {
    "name": "Cherry pick a commit",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/8047) in GitLab 8.15.  Cherry picks a commit to a given branch.",
    "method": "POST",
    "path": "/projects/:id/repository/commits/:sha/cherry_pick",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit hash"
      },
      {
        "name": "branch",
        "type": "string",
        "required": true,
        "description": "The name of the branch"
      },
      {
        "name": "dry_run",
        "type": "boolean",
        "required": false,
        "description": "Does not commit any changes. Default is false. Introduced in GitLab 13.3"
      }
    ]
  },
  {
    "name": "Revert a commit",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/22919) in GitLab 11.5.  Reverts a commit in a given branch.",
    "method": "POST",
    "path": "/projects/:id/repository/commits/:sha/revert",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "Commit SHA to revert"
      },
      {
        "name": "branch",
        "type": "string",
        "required": true,
        "description": "Target branch name"
      },
      {
        "name": "dry_run",
        "type": "boolean",
        "required": false,
        "description": "Does not commit any changes. Default is false. Introduced in GitLab 13.3"
      }
    ]
  },
  {
    "name": "Get the diff of a commit",
    "description": "Get the diff of a commit in a project.",
    "method": "GET",
    "path": "/projects/:id/repository/commits/:sha/diff",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit hash or name of a repository branch or tag"
      }
    ]
  },
  {
    "name": "Get the comments of a commit",
    "description": "Get the comments of a commit in a project.",
    "method": "GET",
    "path": "/projects/:id/repository/commits/:sha/comments",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit hash or name of a repository branch or tag"
      }
    ]
  },
  {
    "name": "Post comment to commit",
    "description": "Adds a comment to a commit.  In order to post a comment in a particular line of a particular file, you must specify the full commit SHA, the `path`, the `line` and `line_type` should be `new`.  The comment will be added at the end of the last commit if at least one of the cases below is valid:  - the `sha` is instead a branch or a tag and the `line` or `path` are invalid - the `line` number is invalid (does not exist) - the `path` is invalid (does not exist)  In any of the above cases, the response of `line`, `line_type` and `path` is set to `null`.",
    "method": "POST",
    "path": "/projects/:id/repository/commits/:sha/comments",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit SHA or name of a repository branch or tag"
      },
      {
        "name": "note",
        "type": "string",
        "required": true,
        "description": "The text of the comment"
      },
      {
        "name": "path",
        "type": "string",
        "required": false,
        "description": "The file path relative to the repository"
      },
      {
        "name": "line",
        "type": "integer",
        "required": false,
        "description": "The line number where the comment should be placed"
      },
      {
        "name": "line_type",
        "type": "string",
        "required": false,
        "description": "The line type. Takes new or old as arguments"
      }
    ]
  },
  {
    "name": "Get the discussions of a commit",
    "description": "Get the discussions of a commit in a project.",
    "method": "GET",
    "path": "/projects/:id/repository/commits/:sha/discussions",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit hash or name of a repository branch or tag"
      }
    ]
  },
  {
    "name": "List the statuses of a commit",
    "description": "List the statuses of a commit in a project. The pagination parameters `page` and `per_page` can be used to restrict the list of references.",
    "method": "GET",
    "path": "/projects/:id/repository/commits/:sha/statuses",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit SHA"
      },
      {
        "name": "ref",
        "type": "string",
        "required": false,
        "description": "The name of a repository branch or tag or, if not given, the default branch"
      },
      {
        "name": "stage",
        "type": "string",
        "required": false,
        "description": "Filter by build stage, e.g., test"
      },
      {
        "name": "name",
        "type": "string",
        "required": false,
        "description": "Filter by job name, e.g., bundler:audit"
      },
      {
        "name": "all",
        "type": "boolean",
        "required": false,
        "description": "Return all statuses, not only the latest ones"
      }
    ]
  },
  {
    "name": "Post the build status to a commit",
    "description": "Adds or updates a build status of a commit.",
    "method": "POST",
    "path": "/projects/:id/statuses/:sha",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit SHA"
      },
      {
        "name": "state",
        "type": "string",
        "required": true,
        "description": "The state of the status. Can be one of the following: pending, running, success, failed, canceled"
      },
      {
        "name": "ref",
        "type": "string",
        "required": false,
        "description": "The ref (branch or tag) to which the status refers"
      },
      {
        "name": "name or context",
        "type": "string",
        "required": false,
        "description": "The label to differentiate this status from the status of other systems. Default value is default"
      },
      {
        "name": "target_url",
        "type": "string",
        "required": false,
        "description": "The target URL to associate with this status"
      },
      {
        "name": "description",
        "type": "string",
        "required": false,
        "description": "The short description of the status"
      },
      {
        "name": "coverage",
        "type": "float",
        "required": false,
        "description": "The total code coverage"
      },
      {
        "name": "pipeline_id",
        "type": "integer",
        "required": false,
        "description": "The ID of the pipeline to set status. Use in case of several pipeline on same SHA."
      }
    ]
  },
  {
    "name": "List Merge Requests associated with a commit",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab-foss/-/merge_requests/18004) in GitLab 10.7.  Get a list of Merge Requests related to the specified commit.",
    "method": "GET",
    "path": "/projects/:id/repository/commits/:sha/merge_requests",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit SHA"
      }
    ]
  },
  {
    "name": "Get GPG signature of a commit",
    "description": "Get the [GPG signature from a commit](../user/project/repository/gpg_signed_commits/index.md), if it is signed. For unsigned commits, it results in a 404 response.",
    "method": "GET",
    "path": "/projects/:id/repository/commits/:sha/signature",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project owned by the authenticated user"
      },
      {
        "name": "sha",
        "type": "string",
        "required": true,
        "description": "The commit hash or name of a repository branch or tag"
      }
    ]
  }
]