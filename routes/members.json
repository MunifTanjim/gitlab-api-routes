[
  {
    "name": "List all members of a group or project",
    "description": "Gets a list of group or project members viewable by the authenticated user. Returns only direct members and not inherited members through ancestors groups.  This function takes pagination parameters `page` and `per_page` to restrict the list of users.",
    "method": "GET",
    "path": "/groups/:id/members",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "query",
        "type": "string",
        "required": false,
        "description": "A query string to search for members"
      },
      {
        "name": "user_ids",
        "type": "array of integers",
        "required": false,
        "description": "Filter the results on the given user IDs"
      }
    ]
  },
  {
    "name": "List all members of a group or project",
    "description": "Gets a list of group or project members viewable by the authenticated user. Returns only direct members and not inherited members through ancestors groups.  This function takes pagination parameters `page` and `per_page` to restrict the list of users.",
    "method": "GET",
    "path": "/projects/:id/members",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "query",
        "type": "string",
        "required": false,
        "description": "A query string to search for members"
      },
      {
        "name": "user_ids",
        "type": "array of integers",
        "required": false,
        "description": "Filter the results on the given user IDs"
      }
    ]
  },
  {
    "name": "List all members of a group or project including inherited members",
    "description": "Gets a list of group or project members viewable by the authenticated user, including inherited members and permissions through ancestor groups.  CAUTION: **Caution:** Due to [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/249523), the users effective `access_level` may actually be higher than returned value when listing group members.  This function takes pagination parameters `page` and `per_page` to restrict the list of users.",
    "method": "GET",
    "path": "/groups/:id/members/all",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "query",
        "type": "string",
        "required": false,
        "description": "A query string to search for members"
      },
      {
        "name": "user_ids",
        "type": "array of integers",
        "required": false,
        "description": "Filter the results on the given user IDs"
      }
    ]
  },
  {
    "name": "List all members of a group or project including inherited members",
    "description": "Gets a list of group or project members viewable by the authenticated user, including inherited members and permissions through ancestor groups.  CAUTION: **Caution:** Due to [an issue](https://gitlab.com/gitlab-org/gitlab/-/issues/249523), the users effective `access_level` may actually be higher than returned value when listing group members.  This function takes pagination parameters `page` and `per_page` to restrict the list of users.",
    "method": "GET",
    "path": "/projects/:id/members/all",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "query",
        "type": "string",
        "required": false,
        "description": "A query string to search for members"
      },
      {
        "name": "user_ids",
        "type": "array of integers",
        "required": false,
        "description": "Filter the results on the given user IDs"
      }
    ]
  },
  {
    "name": "Get a member of a group or project",
    "description": "Gets a member of a group or project. Returns only direct members and not inherited members through ancestor groups.",
    "method": "GET",
    "path": "/groups/:id/members/:user_id",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      }
    ]
  },
  {
    "name": "Get a member of a group or project",
    "description": "Gets a member of a group or project. Returns only direct members and not inherited members through ancestor groups.",
    "method": "GET",
    "path": "/projects/:id/members/:user_id",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      }
    ]
  },
  {
    "name": "Get a member of a group or project, including inherited members",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/17744) in GitLab 12.4.  Gets a member of a group or project, including members inherited through ancestor groups. See the corresponding [endpoint to list all inherited members](#list-all-members-of-a-group-or-project-including-inherited-members) for details.",
    "method": "GET",
    "path": "/groups/:id/members/all/:user_id",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      }
    ]
  },
  {
    "name": "Get a member of a group or project, including inherited members",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/17744) in GitLab 12.4.  Gets a member of a group or project, including members inherited through ancestor groups. See the corresponding [endpoint to list all inherited members](#list-all-members-of-a-group-or-project-including-inherited-members) for details.",
    "method": "GET",
    "path": "/projects/:id/members/all/:user_id",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      }
    ]
  },
  {
    "name": "List all billable members of a group",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/217384) in GitLab 13.5.  Gets a list of group members that count as billable. The list includes members in the subgroup or subproject.  NOTE: Unlike other API endpoints, billable members is updated once per day at 12:00 UTC.  This function takes [pagination](README.md#pagination) parameters `page` and `per_page` to restrict the list of users.",
    "method": "GET",
    "path": "/groups/:id/billable_members",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the group owned by the authenticated user"
      }
    ]
  },
  {
    "name": "Add a member to a group or project",
    "description": "Adds a member to a group or project.",
    "method": "POST",
    "path": "/groups/:id/members",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer/string",
        "required": true,
        "description": "The user ID of the new member or multiple IDs separated by commas"
      },
      {
        "name": "access_level",
        "type": "integer",
        "required": true,
        "description": "A valid access level"
      },
      {
        "name": "expires_at",
        "type": "string",
        "required": false,
        "description": "A date string in the format YEAR-MONTH-DAY"
      }
    ]
  },
  {
    "name": "Add a member to a group or project",
    "description": "Adds a member to a group or project.",
    "method": "POST",
    "path": "/projects/:id/members",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer/string",
        "required": true,
        "description": "The user ID of the new member or multiple IDs separated by commas"
      },
      {
        "name": "access_level",
        "type": "integer",
        "required": true,
        "description": "A valid access level"
      },
      {
        "name": "expires_at",
        "type": "string",
        "required": false,
        "description": "A date string in the format YEAR-MONTH-DAY"
      }
    ]
  },
  {
    "name": "Edit a member of a group or project",
    "description": "Updates a member of a group or project.",
    "method": "PUT",
    "path": "/groups/:id/members/:user_id",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      },
      {
        "name": "access_level",
        "type": "integer",
        "required": true,
        "description": "A valid access level"
      },
      {
        "name": "expires_at",
        "type": "string",
        "required": false,
        "description": "A date string in the format YEAR-MONTH-DAY"
      }
    ]
  },
  {
    "name": "Edit a member of a group or project",
    "description": "Updates a member of a group or project.",
    "method": "PUT",
    "path": "/projects/:id/members/:user_id",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      },
      {
        "name": "access_level",
        "type": "integer",
        "required": true,
        "description": "A valid access level"
      },
      {
        "name": "expires_at",
        "type": "string",
        "required": false,
        "description": "A date string in the format YEAR-MONTH-DAY"
      }
    ]
  },
  {
    "name": "Set override flag for a member of a group",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/4875) in GitLab 13.0.  By default, the access level of LDAP group members is set to the value specified by LDAP through Group Sync. You can allow access level overrides by calling this endpoint.",
    "method": "POST",
    "path": "/groups/:id/members/:user_id/override",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      }
    ]
  },
  {
    "name": "Remove override for a member of a group",
    "description": "> [Introduced](https://gitlab.com/gitlab-org/gitlab/-/issues/4875) in GitLab 13.0.  Sets the override flag to false and allows LDAP Group Sync to reset the access level to the LDAP-prescribed value.",
    "method": "DELETE",
    "path": "/groups/:id/members/:user_id/override",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      }
    ]
  },
  {
    "name": "Remove a member from a group or project",
    "description": "Removes a user from a group or project.",
    "method": "DELETE",
    "path": "/groups/:id/members/:user_id",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      },
      {
        "name": "unassign_issuables",
        "type": "boolean",
        "description": "Flag indicating if the removed member should be unassigned from any issues or merge requests within given group or project"
      }
    ]
  },
  {
    "name": "Remove a member from a group or project",
    "description": "Removes a user from a group or project.",
    "method": "DELETE",
    "path": "/projects/:id/members/:user_id",
    "params": [
      {
        "name": "id",
        "type": "integer/string",
        "required": true,
        "description": "The ID or URL-encoded path of the project or group owned by the authenticated user"
      },
      {
        "name": "user_id",
        "type": "integer",
        "required": true,
        "description": "The user ID of the member"
      },
      {
        "name": "unassign_issuables",
        "type": "boolean",
        "description": "Flag indicating if the removed member should be unassigned from any issues or merge requests within given group or project"
      }
    ]
  }
]