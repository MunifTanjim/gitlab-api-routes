[
  {
    "name": "Create a new Geo node",
    "description": "Creates a new Geo node.",
    "method": "POST",
    "path": "/geo_nodes",
    "params": [
      {
        "name": "primary",
        "type": "boolean",
        "required": false,
        "description": "Specifying whether this node will be primary. Defaults to false."
      },
      {
        "name": "enabled",
        "type": "boolean",
        "required": false,
        "description": "Flag indicating if the Geo node is enabled. Defaults to true."
      },
      {
        "name": "name",
        "type": "string",
        "required": true,
        "description": "The unique identifier for the Geo node. Must match geo_node_name if it is set in gitlab.rb, otherwise it must match external_url"
      },
      {
        "name": "url",
        "type": "string",
        "required": true,
        "description": "The user-facing URL for the Geo node."
      },
      {
        "name": "internal_url",
        "type": "string",
        "required": false,
        "description": "The URL defined on the primary node that secondary nodes should use to contact it. Returns url if not set."
      },
      {
        "name": "files_max_capacity",
        "type": "integer",
        "required": false,
        "description": "Control the maximum concurrency of LFS/attachment backfill for this secondary node. Defaults to 10."
      },
      {
        "name": "repos_max_capacity",
        "type": "integer",
        "required": false,
        "description": "Control the maximum concurrency of repository backfill for this secondary node. Defaults to 25."
      },
      {
        "name": "verification_max_capacity",
        "type": "integer",
        "required": false,
        "description": "Control the maximum concurrency of repository verification for this node. Defaults to 100."
      },
      {
        "name": "container_repositories_max_capacity",
        "type": "integer",
        "required": false,
        "description": "Control the maximum concurrency of container repository sync for this node. Defaults to 10."
      },
      {
        "name": "sync_object_storage",
        "type": "boolean",
        "required": false,
        "description": "Flag indicating if the secondary Geo node will replicate blobs in Object Storage. Defaults to false."
      },
      {
        "name": "selective_sync_type",
        "type": "string",
        "required": false,
        "description": "Limit syncing to only specific groups or shards. Valid values: \"namespaces\", \"shards\", or null."
      },
      {
        "name": "selective_sync_shards",
        "type": "array",
        "required": false,
        "description": "The repository storage for the projects synced if selective_sync_type == shards."
      },
      {
        "name": "selective_sync_namespace_ids",
        "type": "array",
        "required": false,
        "description": "The IDs of groups that should be synced, if selective_sync_type == namespaces."
      },
      {
        "name": "minimum_reverification_interval",
        "type": "integer",
        "required": false,
        "description": "The interval (in days) in which the repository verification is valid. Once expired, it will be reverified. This has no effect when set on a secondary node."
      }
    ]
  },
  {
    "name": "Retrieve configuration about all Geo nodes",
    "description": "",
    "method": "GET",
    "path": "/geo_nodes",
    "params": []
  },
  {
    "name": "Retrieve configuration about a specific Geo node",
    "description": "",
    "method": "GET",
    "path": "/geo_nodes/:id",
    "params": []
  },
  {
    "name": "Edit a Geo node",
    "description": "Updates settings of an existing Geo node.  _This can only be run against a primary Geo node._",
    "method": "PUT",
    "path": "/geo_nodes/:id",
    "params": [
      {
        "name": "id",
        "type": "integer",
        "required": true,
        "description": "The ID of the Geo node."
      },
      {
        "name": "enabled",
        "type": "boolean",
        "required": false,
        "description": "Flag indicating if the Geo node is enabled."
      },
      {
        "name": "name",
        "type": "string",
        "required": true,
        "description": "The unique identifier for the Geo node. Must match geo_node_name if it is set in gitlab.rb, otherwise it must match external_url."
      },
      {
        "name": "url",
        "type": "string",
        "required": true,
        "description": "The user-facing URL of the Geo node."
      },
      {
        "name": "internal_url",
        "type": "string",
        "required": false,
        "description": "The URL defined on the primary node that secondary nodes should use to contact it. Returns url if not set."
      },
      {
        "name": "files_max_capacity",
        "type": "integer",
        "required": false,
        "description": "Control the maximum concurrency of LFS/attachment backfill for this secondary node."
      },
      {
        "name": "repos_max_capacity",
        "type": "integer",
        "required": false,
        "description": "Control the maximum concurrency of repository backfill for this secondary node."
      },
      {
        "name": "verification_max_capacity",
        "type": "integer",
        "required": false,
        "description": "Control the maximum concurrency of verification for this node."
      },
      {
        "name": "container_repositories_max_capacity",
        "type": "integer",
        "required": false,
        "description": "Control the maximum concurrency of container repository sync for this node."
      },
      {
        "name": "sync_object_storage",
        "type": "boolean",
        "required": false,
        "description": "Flag indicating if the secondary Geo node will replicate blobs in Object Storage."
      },
      {
        "name": "selective_sync_type",
        "type": "string",
        "required": false,
        "description": "Limit syncing to only specific groups or shards. Valid values: \"namespaces\", \"shards\", or null."
      },
      {
        "name": "selective_sync_shards",
        "type": "array",
        "required": false,
        "description": "The repository storage for the projects synced if selective_sync_type == shards."
      },
      {
        "name": "selective_sync_namespace_ids",
        "type": "array",
        "required": false,
        "description": "The IDs of groups that should be synced, if selective_sync_type == namespaces."
      },
      {
        "name": "minimum_reverification_interval",
        "type": "integer",
        "required": false,
        "description": "The interval (in days) in which the repository verification is valid. Once expired, it will be reverified. This has no effect when set on a secondary node."
      }
    ]
  },
  {
    "name": "Delete a Geo node",
    "description": "Removes the Geo node.  NOTE: **Note:** Only a Geo primary node will accept this request.",
    "method": "DELETE",
    "path": "/geo_nodes/:id",
    "params": [
      {
        "name": "id",
        "type": "integer",
        "required": true,
        "description": "The ID of the Geo node."
      }
    ]
  },
  {
    "name": "Repair a Geo node",
    "description": "To repair the OAuth authentication of a Geo node.  _This can only be run against a primary Geo node._",
    "method": "POST",
    "path": "/geo_nodes/:id/repair",
    "params": []
  },
  {
    "name": "Retrieve status about all Geo nodes",
    "description": "",
    "method": "GET",
    "path": "/geo_nodes/status",
    "params": []
  },
  {
    "name": "Retrieve status about a specific Geo node",
    "description": "",
    "method": "GET",
    "path": "/geo_nodes/:id/status",
    "params": []
  },
  {
    "name": "Retrieve project sync or verification failures that occurred on the current node",
    "description": "This only works on a secondary node.",
    "method": "GET",
    "path": "/geo_nodes/current/failures",
    "params": [
      {
        "name": "type",
        "type": "string",
        "required": false,
        "description": "Type of failed objects (repository/wiki)"
      },
      {
        "name": "failure_type",
        "type": "string",
        "required": false,
        "description": "Type of failures (sync/checksum_mismatch/verification)"
      }
    ]
  }
]